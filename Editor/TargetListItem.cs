using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;

namespace HyperSwitcher
{
    public class TargetListItem
    {
        public static TargetListItem[] All()
        {
            List<TargetListItem> res = new List<TargetListItem>();

            res.Add(new TargetListItem() { 
                DisplayName = "Windows x64",
                Group = BuildTargetGroup.Standalone,
                Target = BuildTarget.StandaloneWindows64,
                Icon = ResourceLoader.Load<Texture>("icons/win10"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "Windows x32",
                Group = BuildTargetGroup.Standalone,
                Target = BuildTarget.StandaloneWindows,
                Icon = ResourceLoader.Load<Texture>("icons/win10"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "Linux x64",
                Group = BuildTargetGroup.Standalone,
                Target = BuildTarget.StandaloneLinux64,
                Icon = ResourceLoader.Load<Texture>("icons/ubuntu"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "Linux x32",
                Group = BuildTargetGroup.Standalone,
                Target = BuildTarget.StandaloneLinux,
                Icon = ResourceLoader.Load<Texture>("icons/ubuntu"),
            });
            #if !UNITY_2019_2_OR_NEWER
            res.Add(new TargetListItem() { 
                DisplayName = "Linux Universal",
                Group = BuildTargetGroup.Standalone,
                Target = BuildTarget.StandaloneLinuxUniversal,
                Icon = ResourceLoader.Load<Texture>("icons/ubuntu"),
            });
            #endif
            res.Add(new TargetListItem() { 
                DisplayName = "OS X",
                Group = BuildTargetGroup.Standalone,
                Target = BuildTarget.StandaloneOSX,
                Icon = ResourceLoader.Load<Texture>("icons/osx"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "Android",
                Group = BuildTargetGroup.Android,
                Target = BuildTarget.Android,
                Icon = ResourceLoader.Load<Texture>("icons/android"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "iOS",
                Group = BuildTargetGroup.iOS,
                Target = BuildTarget.iOS,
                Icon = ResourceLoader.Load<Texture>("icons/ios"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "tvOS",
                Group = BuildTargetGroup.tvOS,
                Target = BuildTarget.tvOS,
                Icon = ResourceLoader.Load<Texture>("icons/tvos"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "Switch",
                Group = BuildTargetGroup.Switch,
                Target = BuildTarget.Switch,
                Icon = ResourceLoader.Load<Texture>("icons/switch"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "PS4",
                Group = BuildTargetGroup.PS4,
                Target = BuildTarget.PS4,
                Icon = ResourceLoader.Load<Texture>("icons/ps4"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "Xbox One",
                Group = BuildTargetGroup.XboxOne,
                Target = BuildTarget.XboxOne,
                Icon = ResourceLoader.Load<Texture>("icons/xboxone"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "WebGL",
                Group = BuildTargetGroup.WebGL,
                Target = BuildTarget.WebGL,
                Icon = ResourceLoader.Load<Texture>("icons/webgl"),
            });
            res.Add(new TargetListItem() { 
                DisplayName = "Facebook",
                Group = BuildTargetGroup.Facebook,
                Target = BuildTarget.NoTarget,
                Icon = ResourceLoader.Load<Texture>("icons/facebook"),
            });

            return res.ToArray();
        }

        public string DisplayName
        {
            get; private set;
        }

        public bool IsCached
        {
            get; private set;
        }

        public bool IsSupported
        {
            get {
                return ModuleManager.Instance.IsPlatformSupportLoadedByBuildTarget(Target);
            }
        }

        public bool IsActive
        {
            get {
                return EditorUserBuildSettings.activeBuildTarget == Target;
            }
        }

        public BuildTargetGroup Group
        {
            get; private set;
        }

        public Texture Icon
        {
            get; private set;
        } = null;

        public BuildTarget Target
        {
            get; private set;
        }

        public void Switch()
        {
            SwitchUtility.SwitchActiveBuildTarget(Group, Target);
        }
    }
}
