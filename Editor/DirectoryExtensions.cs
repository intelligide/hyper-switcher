using System.IO;
using UnityEngine;

namespace HyperSwitcher
{
    static class DirectoryExtensions
    {
        public static void Copy(string SourceDirName, string DestDirName, bool CopySubDirs = true)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(SourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: " + SourceDirName
                );
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(DestDirName))
            {
                Directory.CreateDirectory(DestDirName);
            }
            
            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = longPath(Path.Combine(DestDirName, file.Name));
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (CopySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(DestDirName, subdir.Name);
                    Copy(subdir.FullName, temppath, CopySubDirs);
                }
            }
        }

        private static string longPath(string path)
        {
            return @"\\?\" + path;
        }

        public static void Clear(string FolderName) 
        {
            foreach(string file in Directory.GetFiles(FolderName, "*", SearchOption.AllDirectories))
            {
                string filelong = longPath(file);
                var attr = File.GetAttributes(filelong);
                if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    File.SetAttributes(filelong, attr ^ FileAttributes.ReadOnly);
                }
                File.Delete(filelong);
            }
        }
    }
}