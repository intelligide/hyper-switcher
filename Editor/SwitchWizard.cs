using UnityEditor;
using UnityEngine;

#if UNITY_2019_1_OR_NEWER
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine.Assertions;
#endif

namespace HyperSwitcher
{
    public class SwitchWizard : EditorWindow 
    {
        [MenuItem("Tools/Switch platform")]
        static void CreateWizard() 
        {
            SwitchWizard wnd = GetWindow<SwitchWizard>();
            wnd.titleContent = new GUIContent("SwitchWizard");
        }

#if UNITY_2019_1_OR_NEWER

        private UI.TabBar Bar;

        private List<VisualElement> Pages = new List<VisualElement>();

        public void OnEnable()
        {
            // Each editor window contains a root VisualElement object
            VisualElement root = rootVisualElement;

            var styleSheet = ResourceLoader.LoadStyleSheet<SwitchWizard>();
            root.styleSheets.Add(styleSheet);

            // Import UXML
            var visualTree = ResourceLoader.LoadVisualTree<SwitchWizard>();
            VisualElement UXMLElement = visualTree.CloneTree();
            root.Add(UXMLElement);
            UXMLElement.style.flexGrow = 1;

            UI.SwitchPage switchPage = UXMLElement.Query<UI.SwitchPage>("switch-page");
            Assert.IsNotNull(switchPage);
            Pages.Add(switchPage);
            switchPage.style.flexGrow = 1;

            UI.SettingsPage settingsPage = UXMLElement.Query<UI.SettingsPage>("settings-page");
            Assert.IsNotNull(settingsPage);
            Pages.Add(settingsPage);
            settingsPage.style.flexGrow = 1;
            settingsPage.style.display = DisplayStyle.None;

            Bar = UXMLElement.Query<UI.TabBar>("tabs");
            Assert.IsNotNull(Bar);
            Bar.AddTab("Switch");
            Bar.AddTab("Settings");

            Bar.OnCurrentChanged += ChangePage;
        }

        private void ChangePage(int previousValue, int newValue)
        {
            if(previousValue > -1) 
            {
                Pages[previousValue].style.display = DisplayStyle.None;
            }
            Pages[newValue].style.display = DisplayStyle.Flex;
        }

#else

        private bool m_customCachePathEnabled;
        private string m_cachePath;
        private void OnEnable() 
        {
            m_customCachePathEnabled = Prefs.IsUsingCustomCachePath();
            m_cachePath = Prefs.GetCachePath();
        }

        int selectedTab;
        void OnGUI()
        {
            selectedTab = GUILayout.Toolbar (selectedTab, new string[] { "Switch", "Config" });

            switch(selectedTab) {
                default:
                case 0:
                    switchPage();
                    break;
                case 1:
                    configPage();
                    break;
            }
        }

        Vector2 scrollPos;
        bool showUnavailableTargets = false;
        void switchPage() {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

            GUILayout.Label("Platforms", EditorStyles.boldLabel);

            foreach(TargetListItem target in TargetListItem.All()) 
            {
                if(target.IsSupported || (!target.IsSupported && showUnavailableTargets)) 
                {
                    float rowHeigth = 30; 
                    EditorGUILayout.BeginHorizontal(new GUIStyle() { padding = new RectOffset(5, 5, 0, 0) }, GUILayout.Height(rowHeigth));
                    GUILayout.Label(target.Icon, new GUIStyle() { alignment = TextAnchor.MiddleLeft, padding = new RectOffset(2, 2, 2, 2) }, GUILayout.Width(30), GUILayout.Height(rowHeigth));
                    GUILayout.Label(target.DisplayName, new GUIStyle() { alignment = TextAnchor.MiddleLeft }, GUILayout.Height(rowHeigth));
                    if(target.IsActive)
                    {
                        GUILayout.Label(ResourceLoader.Load<Texture2D>("icons/unity"), new GUIStyle() { alignment = TextAnchor.MiddleLeft, padding = new RectOffset(0, 0, 2, 2) }, GUILayout.Width(50), GUILayout.Height(rowHeigth));
                    }

                    if(target.IsCached) 
                    {
                        GUI.color = Color.green; 
                    }
                    else 
                    {
                        GUI.color = Color.grey; 
                    }
                    GUILayout.Label(ResourceLoader.Load<Texture2D>("icons/database"), new GUIStyle() { alignment = TextAnchor.MiddleLeft, padding = new RectOffset(0, 0, 2, 2) }, GUILayout.Width(40), GUILayout.Height(rowHeigth));
                    GUI.color = Color.white;


                    GUI.enabled = target.IsSupported;
                    if(GUILayout.Button("Switch...", GUILayout.Width(100), GUILayout.Height(rowHeigth - 2)) && target.IsSupported) 
                    {
                        target.Switch();
                    }
                    GUI.enabled = true;

                    EditorGUILayout.EndHorizontal();
                }                
            }

            showUnavailableTargets = EditorGUILayout.Toggle("Show unavailable targets", showUnavailableTargets);

            EditorGUILayout.EndScrollView();
        }

        void configPage() {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

            GUILayout.Label("General Settings", EditorStyles.boldLabel);

            m_customCachePathEnabled = EditorGUILayout.BeginToggleGroup("Custom Cache Path", m_customCachePathEnabled);
            EditorGUILayout.BeginHorizontal();
            m_cachePath = EditorGUILayout.TextField(m_cachePath);
            if(GUILayout.Button("Browse...")) 
            {
                m_cachePath = EditorUtility.OpenFolderPanel("Select Cache Path", "", "");
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndToggleGroup();

            EditorGUILayout.EndScrollView();

            if (GUI.changed)
            {
                Prefs.UseCustomCachePath(m_customCachePathEnabled);
                Prefs.SetCachePath(m_cachePath);
            }
        }
#endif
     
    }
}
