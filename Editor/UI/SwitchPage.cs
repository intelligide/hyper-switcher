#if UNITY_2019_1_OR_NEWER

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace HyperSwitcher.UI
{
    class SwitchPage : VisualElement
    {
        private TargetListItem[] items;

        private ListView listView;

        public class Factory : UxmlFactory<SwitchPage, Traits> {}

        public class Traits : VisualElement.UxmlTraits
        {
            public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
            {
                get { yield break; }
            }

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);
                SwitchPage page = (SwitchPage) ve;

                VisualElement root = page;
                root.style.flexDirection = FlexDirection.Row;

                page.items = TargetListItem.All();

                page.listView = new ListView(page.items, 30, page.MakeItem, page.BindItem);
                page.listView.selectionType = SelectionType.None;
                page.listView.style.flexGrow = 1;

                root.Add(page.listView);
            }
        }

        private VisualElement MakeItem()
        {
            return new UI.TargetListItemView();
        }

        private void BindItem(VisualElement e, int i)
        {
            UI.TargetListItemView itemView = (UI.TargetListItemView) e;
            Assert.IsNotNull(itemView);
            itemView.Item = items[i];
        }
    }
}

#endif
