#if UNITY_2019_1_OR_NEWER

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace HyperSwitcher.UI
{
    class TargetListItemView : VisualElement
    {
        TargetListItem m_item = null;
        public TargetListItem Item { 
            get  {
                return m_item;
            } 
            set {
                if(m_item != null) 
                {
                    SwitchButton.clickable.clicked -= m_item.Switch;
                }
                m_item = value;

                IconImage.image = m_item.Icon;

                NameLabel.text = m_item.DisplayName;

                if(m_item.IsSupported) 
                {
                    SwitchButton.SetEnabled(true);
                    SwitchButton.tooltip = "";
                }
                else 
                {
                    SwitchButton.SetEnabled(false);
                    SwitchButton.tooltip = m_item.Target + " Support is not available.";         
                }

                CurrentPlatformImage.visible = m_item.IsActive;

                if(m_item.IsCached) 
                {
                    CacheImage.tintColor = Color.green; 
                }
                else 
                {
                    CacheImage.tintColor = Color.grey; 
                }

                SwitchButton.clickable.clicked += m_item.Switch;
            } 
        }

        public Image IconImage { get; private set; } = null;
        public Label NameLabel { get; private set; } = null;
        public Button SwitchButton { get; private set; } = null;
        public Image CacheImage { get; private set; } = null;
        public Image CurrentPlatformImage { get; private set; } = null;

        private Action currentClickCallback;

        public class Factory : UxmlFactory<TargetListItemView, Traits> {}

        public class Traits : VisualElement.UxmlTraits
        {
            public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
            {
                get { yield break; }
            }

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);
                TargetListItemView item = (TargetListItemView) ve;
                item.Init();
            }
        }

        public TargetListItemView() : base()
        {
            Init();
        }

        private bool initialized = false;
        private void Init()
        {
            if(initialized)
            {
                return;
            }

            var styleSheet = ResourceLoader.LoadStyleSheet<TargetListItemView>();
            styleSheets.Add(styleSheet);

            IconImage = new Image();
            IconImage.scaleMode = ScaleMode.ScaleToFit;
            IconImage.name = "icon";
            Add(IconImage);

            NameLabel = new Label();
            NameLabel.name = "target-label";
            Add(NameLabel);

            CurrentPlatformImage = new Image();
            CurrentPlatformImage.image = ResourceLoader.Load<Texture2D>("icons/unity");
            CurrentPlatformImage.scaleMode = ScaleMode.ScaleToFit;
            CurrentPlatformImage.name = "current-platform-indicator";
            Add(CurrentPlatformImage);

            CacheImage = new Image();
            CacheImage.image = ResourceLoader.Load<Texture2D>("icons/database");
            CacheImage.scaleMode = ScaleMode.ScaleToFit;
            CacheImage.name = "cache-indicator";
            CacheImage.tintColor = Color.grey; 
            Add(CacheImage);

            SwitchButton = new Button() {
                text = "Switch...",
            };
            SwitchButton.name = "switch-btn";
            Add(SwitchButton);
            initialized = true;
        }
    }
}

#endif
