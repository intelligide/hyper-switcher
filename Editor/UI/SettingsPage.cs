#if UNITY_2019_1_OR_NEWER

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace HyperSwitcher.UI
{
    class SettingsPage : VisualElement
    {
        /*
         * Fields 
         */
        private UI.FileField cachePathField = null;

        public class Factory : UxmlFactory<SettingsPage, Traits> {}

        public class Traits : VisualElement.UxmlTraits
        {
            public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
            {
                get { yield break; }
            }

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);

                SettingsPage page = (SettingsPage) ve;

                var visualTree = ResourceLoader.LoadVisualTree<SettingsPage>();
                VisualElement UXMLElement = visualTree.CloneTree();
                ve.Add(UXMLElement);

                page.cachePathField = UXMLElement.Query<UI.FileField>("cachePathField");
                page.cachePathField.Value = Prefs.GetCachePath();
                page.cachePathField.SetEnabled(Prefs.IsUsingCustomCachePath());

                page.cachePathField.RegisterCallback<ChangeEvent<string>>((ChangeEvent<string> evt) => {
                    Prefs.SetCachePath(page.cachePathField.Value);
                });

                Toggle t = UXMLElement.Query<Toggle>("customCachePathToggle");
                t.value = Prefs.IsUsingCustomCachePath();
                t.RegisterCallback<ChangeEvent<bool>>((ChangeEvent<bool> evt) => {
                    Prefs.UseCustomCachePath(evt.newValue);
                    page.cachePathField.SetEnabled(evt.newValue);
                });
            }
        }
    }
}

#endif
