#if UNITY_2019_1_OR_NEWER

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace HyperSwitcher.UI
{
    class TabBar : VisualElement
    {

        public event Action<int, int> OnCurrentChanged;
        
        public int CurrentIndex { get; private set; } = -1;

        private List<Button> Buttons = new List<Button>();

        public class Factory : UxmlFactory<TabBar, Traits> {}

        public class Traits : VisualElement.UxmlTraits
        {           
            public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
            {
                get { yield break; }
            }

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);

                TabBar bar = (TabBar) ve;

                var styleSheet = ResourceLoader.LoadStyleSheet<TabBar>();
                ve.styleSheets.Add(styleSheet);
            }
        }


        public void AddTab(string text)
        {
            var btn = new Button();
            btn.text = text;
            AddTab(btn);
        }

        public void AddTab(Button btn)
        {
            this.Add(btn);

            int idx = Buttons.Count;
            Buttons.Add(btn);

            btn.clickable.clicked += () => {
                Buttons[CurrentIndex].RemoveFromClassList("active");
                var old = CurrentIndex;
                CurrentIndex = idx;
                btn.AddToClassList("active");

                OnCurrentChanged.Invoke(old, CurrentIndex);
            };

            if (idx == 0)
            {
                btn.AddToClassList("first-child");
                btn.AddToClassList("active");
                btn.AddToClassList("last-child");
                CurrentIndex = 0;
            }
            else
            {
                Buttons[idx - 1].RemoveFromClassList("last-child");
                btn.AddToClassList("last-child");
            }
        }
    }
}

#endif
