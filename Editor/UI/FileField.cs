#if UNITY_2019_1_OR_NEWER

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace HyperSwitcher.UI
{
    class FileField : VisualElement
    {

        enum EFileType {
            Dir,
            File,
        }

        private string m_value = "";
        public string Value {
            get {
                return m_value;
            }
            set {
                if (m_value != value) 
                {
                    this.SendEvent(ChangeEvent<string>.GetPooled(m_value, value));
                }
                m_value = value;
                if(pathField != null) 
                {
                    pathField.value = value;
                }
            }
        }

        private EFileType fileType;

        private string[] filters;

        /*
         * Fields 
         */
        private TextField pathField = null;
        private Button browseButton = null;

        public class Factory : UxmlFactory<FileField, Traits> {}

        public class Traits : VisualElement.UxmlTraits
        {
            UxmlStringAttributeDescription valueAttr = new UxmlStringAttributeDescription { 
                name = "value",
                defaultValue = "",
                use = UxmlAttributeDescription.Use.Optional,
            };

            UxmlStringAttributeDescription filtersAttr = new UxmlStringAttributeDescription { 
                name = "filters",
                defaultValue = "",
                use = UxmlAttributeDescription.Use.Optional,
            };

            UxmlEnumAttributeDescription<EFileType> typeAttr = new UxmlEnumAttributeDescription<EFileType> {
                name = "type",
                defaultValue = EFileType.File,
                use = UxmlAttributeDescription.Use.Optional,
            };
            
            public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
            {
                get { yield break; }
            }

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);

                FileField field = (FileField) ve;

                var styleSheet = ResourceLoader.LoadStyleSheet<FileField>();
                ve.styleSheets.Add(styleSheet);

                var visualTree = ResourceLoader.LoadVisualTree<FileField>();
                VisualElement UXMLElement = visualTree.CloneTree();
                ve.Add(UXMLElement);

                field.pathField = UXMLElement.Query<TextField>("pathfield").First();
                field.pathField.RegisterCallback<FocusOutEvent>((FocusOutEvent evt) => {
                    field.Value = field.pathField.value;
                });

                field.browseButton = UXMLElement.Query<Button>("browsebutton").First();
                field.browseButton.clickable.clicked += field.SelectFolder;

                field.Value = valueAttr.GetValueFromBag(bag, cc);
                field.fileType = typeAttr.GetValueFromBag(bag, cc);
                field.filters = ParseFilters(filtersAttr.GetValueFromBag(bag, cc));
            }

            private static string[] ParseFilters(string filterStr)
            {
                filterStr = filterStr.Trim();
                if(filterStr.Length == 0) {
                    return new string[0];
                }
                string[] filters = filterStr.Split(new char[] { '|' });
                string[] res = new string[filters.Length];
                foreach(string filter in filters)
                for(int i = 0; i < filters.Length; i++)
                {
                    string[] f = filters[i].Split(new char[] { ';' });
                    if(f.Length != 2) 
                    {
                        throw new ArgumentException("Bad filter string");
                    }
                    res[i * 2] = f[0];
                    res[i * 2 + 1] = f[1];
                }

                return res;
            }
        }

        private void SelectFolder() 
        {
            string res;
            if(fileType == EFileType.Dir) 
            {
                res = EditorUtility.OpenFolderPanel("Select Cache Path", "", "");
            }
            else
            {
                res = EditorUtility.OpenFilePanelWithFilters("Select Cache Path", "", filters);
            }

            if(res != "") {
                Value = res; 
            }
        }
    }
}

#endif
