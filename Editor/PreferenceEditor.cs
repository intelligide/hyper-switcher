#if !UNITY_2019_1_OR_NEWER

using UnityEngine;
using UnityEditor;

namespace HyperSwitcher
{
    class PreferenceEditor
    {
        // Have we loaded the prefs yet
        private static bool prefsLoaded = false;

        private static bool enableTestFeatures;

        [PreferenceItem("Hyper Switcher")]
        private static void preferencesGUI()
        {
            if (!prefsLoaded)
            {
                enableTestFeatures = Prefs.Global.GetBool("EnableTestFeatures", false);
                prefsLoaded = true;
            }
    
            EditorGUILayout.LabelField("Version: " + Constants.Version);
            enableTestFeatures = EditorGUILayout.Toggle("Enable test features", enableTestFeatures);
    
            if (GUI.changed)
            {
                Prefs.Global.SetBool("EnableTestFeatures", enableTestFeatures);
            }
        }
    }
}

#endif