
namespace HyperSwitcher
{
    static class Constants
    {
        public const string Version = "1.0.0a1";

        public static class Prefs
        {
            public const string CachePathKey = "CachePath";
        }
    }
}