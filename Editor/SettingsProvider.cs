#if UNITY_2019_1_OR_NEWER

using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UIElements;

namespace HyperSwitcher
{
    static class HyperSwitcherSettingsProvider
    {
        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            var provider = new SettingsProvider("Preferences/HyperSwitcherSettings", SettingsScope.User)
            {
                label = "Hyper Switcher",
                // activateHandler is called when the user clicks on the Settings item in the Settings window.
                activateHandler = (searchContext, rootElement) =>
                {
                    var container = new VisualElement();
                    rootElement.Add(container);
                    container.AddToClassList("hs-settings-panel");

                    var styleSheet = ResourceLoader.LoadStyleSheet(typeof(HyperSwitcherSettingsProvider));
                    rootElement.styleSheets.Add(styleSheet);
                    
                    var title = new Label()
                    {
                        text = "Hyper Switcher",
                    };
                    title.name = "title";
                    container.Add(title);

                    var versionLbl = new Label()
                    {
                        text = "Version: " + Constants.Version,
                    };
                    container.Add(versionLbl);

                    var testFeaturesToggle = new Toggle()
                    {
                        text = "Enable Test Features.",
                        value = Prefs.Global.GetBool("EnableTestFeatures", false),
                    };
                    container.Add(testFeaturesToggle);
                    testFeaturesToggle.RegisterValueChangedCallback((ChangeEvent<bool> evt) => {
                        Prefs.Global.SetBool("EnableTestFeatures", evt.newValue);
                    });
                },

                // Populate the search keywords to enable smart search filtering and label highlighting:
                keywords = new HashSet<string>(new[] { "Hyper", "Switcher" })
            };

            return provider;
        }
    }
}

#endif