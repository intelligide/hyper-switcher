using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace HyperSwitcher
{
    class ModuleManager
    {
        private static ModuleManager m_Instance=null;

        public static ModuleManager Instance
        {
            get
            {
                if (m_Instance==null)
                {
                    m_Instance = new ModuleManager();
                }
                return m_Instance;
            }
        }

        private ModuleManager()
        {
        }

        private System.Type m_moduleLoader = null;
        private System.Type GetModuleLoader()
        {
            if(m_moduleLoader == null) 
            {
                m_moduleLoader = System.Type.GetType("UnityEditor.Modules.ModuleManager,UnityEditor.dll");
            }
            return m_moduleLoader;
        }

        private MethodInfo m_isPlatformSupportLoadedMethod = null;
        public bool IsPlatformSupportLoaded(string Target)
        {
            if(m_isPlatformSupportLoadedMethod == null) 
            {
                m_isPlatformSupportLoadedMethod = GetModuleLoader().GetMethod("IsPlatformSupportLoaded", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            }
     
            return (bool) m_isPlatformSupportLoadedMethod.Invoke(null,new object[] { Target });
        }
        
#if UNITY_2019_1_OR_NEWER
        private MethodInfo m_isPlatformSupportLoadedByBuildTargetMethod = null;
        public bool IsPlatformSupportLoadedByBuildTarget(BuildTarget buildTarget)
        {
            if(m_isPlatformSupportLoadedMethod == null) 
            {
                m_isPlatformSupportLoadedByBuildTargetMethod = GetModuleLoader().GetMethod("IsPlatformSupportLoadedByBuildTarget", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            }

            return (bool) m_isPlatformSupportLoadedByBuildTargetMethod.Invoke(null,new object[] {
                buildTarget
            });
        }
#else
        private MethodInfo m_getTargetStringFromBuildTargetMethod = null;
        public bool IsPlatformSupportLoadedByBuildTarget(BuildTarget buildTarget)
        {
            if(m_getTargetStringFromBuildTargetMethod == null) 
            {
                m_getTargetStringFromBuildTargetMethod = GetModuleLoader().GetMethod("GetTargetStringFromBuildTarget", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            }

            return IsPlatformSupportLoaded((string) m_getTargetStringFromBuildTargetMethod.Invoke(null,new object[] {
                buildTarget
            }));
        }
#endif
    }
}