using UnityEngine;
using UnityEditor;

namespace HyperSwitcher
{
    class Prefs
    {
        private static Prefs globalPrefs = null;
        public static Prefs Global
        {
            get
            {
                if (globalPrefs == null)
                {
                    globalPrefs = new Prefs();
                }
                return globalPrefs;
            }
        }

        private static Prefs projectPrefs = null;
        public static Prefs Project
        {
            get
            {
                if (projectPrefs == null)
                {
                    projectPrefs = new Prefs(Application.productName);
                }
                return projectPrefs;
            }
        }

        // Namespace
        string _ns;

        private Prefs(string ns = null)
        {
            _ns = ns;
        }

        private string GetNamespacedKey(string key)
        {
            return string.Join(".", new string[]{ "HyperSwitcher", _ns, key });
        }

        public void DeleteKey(string key)
        {
            UnityEditor.EditorPrefs.DeleteKey(GetNamespacedKey(key));
        }

        public string GetString(string key)
        {
            return UnityEditor.EditorPrefs.GetString(GetNamespacedKey(key));
        }

        public string GetString(string key, string defaultValue)
        {
            return UnityEditor.EditorPrefs.GetString(GetNamespacedKey(key), defaultValue);
        }

        public void SetString(string key, string value)
        {
            UnityEditor.EditorPrefs.SetString(GetNamespacedKey(key), value);
        }

        public bool GetBool(string key)
        {
            return UnityEditor.EditorPrefs.GetBool(GetNamespacedKey(key));
        }

        public bool GetBool(string key, bool defaultValue)
        {
            return UnityEditor.EditorPrefs.GetBool(GetNamespacedKey(key), defaultValue);
        }

        public void SetBool(string key, bool value)
        {
            UnityEditor.EditorPrefs.SetBool(GetNamespacedKey(key), value);
        }

        public static void UseCustomCachePath(bool b)
        {
            Project.SetBool("UsingCustomCachePath", b);
        }

        public static bool IsUsingCustomCachePath()
        {
            return Project.GetBool("UsingCustomCachePath");
        }

        public static string GetCachePath()
        {
            return Project.GetString(Constants.Prefs.CachePathKey, "PlatformLibraryCache");
        }

        public static void SetCachePath(string value)
        {
            Project.SetString(Constants.Prefs.CachePathKey, value);
        }
    }
}
