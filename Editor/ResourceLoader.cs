using System;
using UnityEngine;

#if UNITY_2019_1_OR_NEWER
using UnityEngine.UIElements;
#endif

namespace HyperSwitcher
{
    public static class ResourceLoader 
    {
        private static readonly string BaseDirectory = "HyperSwitcher";

        public static T Load<T>(string n) where T : UnityEngine.Object
        {
            return Resources.Load<T>(String.Join("/", new string[] { BaseDirectory, n }));
        }

        private static string NamespaceToPath(string ns)
        {
            if(ns.StartsWith("HyperSwitcher.")) 
            {
                ns = ns.Substring(14); // We remove the first
            }
            return ns.Replace('.', '/');
        }

        #if UNITY_2019_1_OR_NEWER

        private static readonly string StyleSheetDirectory = "stylesheets";

        /*
         * StyleSheet
         */
        public static StyleSheet LoadStyleSheet(string styleSheet) 
        {
            return Resources.Load<StyleSheet>(String.Join("/", new string[] { BaseDirectory, StyleSheetDirectory, styleSheet }));
        }

        public static StyleSheet LoadStyleSheet(Type styleSheetType) 
        {
            return LoadStyleSheet(styleSheetType.Name);
        }

        public static StyleSheet LoadStyleSheet<T>() 
        {
            return LoadStyleSheet(typeof(T));
        }

        private static readonly string VisualTreeDirectory = "uxml";

        /*
         * UXML
         */
        public static VisualTreeAsset LoadVisualTree(string visualTreeName) 
        {
            return Resources.Load<VisualTreeAsset>(String.Join("/", new string[] { BaseDirectory, VisualTreeDirectory, visualTreeName }));
        }

        public static VisualTreeAsset LoadVisualTree(Type visualElementType) 
        {
            return LoadVisualTree(visualElementType.Name);
        }

        public static VisualTreeAsset LoadVisualTree<T>() 
        {
            return LoadVisualTree(typeof(T));
        }

        #endif        
    }
}