using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.IO;

namespace HyperSwitcher
{
    public static class SwitchUtility
    {
        public static void SwitchActiveBuildTarget(BuildTargetGroup buildTargetGroup, BuildTarget buildTarget)
        {
            string cachePath = Prefs.GetCachePath();

            if (EditorUserBuildSettings.activeBuildTarget == buildTarget) 
            {
                Debug.LogWarning("You set the same next platform than the current one !");
                return;
            }

            if (!ModuleManager.Instance.IsPlatformSupportLoadedByBuildTarget(buildTarget)) 
            {
                Debug.LogError("Platform not supported !");
                return;
            }


            Debug.Log("Caching Library dir...");
            //save current Library folder state
            string oldDir = Path.Combine(cachePath, "Library-" + EditorUserBuildSettings.activeBuildTarget);
            if (Directory.Exists(oldDir))
            {
                DirectoryExtensions.Clear(oldDir);
            }
            DirectoryExtensions.Copy("Library", oldDir);

            
            //restore new target Library folder state
            if (Directory.Exists("Library-" + buildTarget)) 
            {
                Debug.Log("Restoring Library for " + buildTarget + "...");
                DirectoryExtensions.Clear("Library");
                Directory.Delete("Library", true);

                Directory.Move("Library-" + buildTarget, "Library");
            }
     
            Debug.Log("Switching to " + buildTarget + "...");
            EditorUserBuildSettings.SwitchActiveBuildTarget(buildTargetGroup, buildTarget);
        }
    }
}
