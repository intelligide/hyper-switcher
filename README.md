# Hyper Switcher

Compatible with Unity 2018.1 and above.

Hyper Switcher aims at reducing the time taken switching platforms by caching all data that has already been imported
and does not need to be re-imported. On a multi-platform project, you often have to switch between platforms to test
compatibility (and other reasons). But the larger your project gets, the longer the switch time becomes because Unity
re-import each asset and it can become a serious setback in your development process.

Hyper Switcher does not modify or interfere with the asset import pipeline. Hyper Switcher copy and move Library folder
into a cache Folder.

It does, however, cache assets that have already been imported so you don’t have to re-import them if you haven’t
modified them. Concretely, the first time you switch to a new platform, you won’t see any benefit. Only subsequent
switches will use cache.

Works with Windows, Mac and Linux editors!

## Installation

Add this to your project's `manifest.json`:

```json
{
    "dependencies": {
        "com.arsenstudio.hyper-switcher": "https://gitlab.com/intelligide/hyper-switcher.git#1.0.0"
    }
}
```

For more information about using packages in general, see [Unity Manual](https://docs.unity3d.com/Manual/Packages.html).

## Supported platforms

- Windows
- Linux
- macOS
- Android
- iOS
- tvOS
- WebGL
- PS4
- Switch
- Xbox One
- Facebook

![alt text](Documentation~/switch.png)

![alt text](Documentation~/config.png)

## License

[MIT License](LICENSE.txt)
